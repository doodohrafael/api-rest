-- criar tabelas
create table ponto (
	id bigint not null auto_increment,
    nome varchar(100),
    xid varchar(20),
    
    primary key (id)
)engine=InnoDB default charset=utf8mb4;

create table dado (
	id bigint not null auto_increment,
	dado double,
    ts datetime,
    ponto_id bigint,
    
    primary key (id)
) engine=InnoDB default charset=utf8mb4;

alter table dado add constraint fk_dado_ponto
foreign key (ponto_id) references ponto (id);

-- dados de inserção para testes do import.sql
insert into ponto (nome, xid) values ('[Ambiente] Armazém 03', 'ID0001_TEMP');
insert into ponto (nome, xid) values ('[Umidade 01] Sala Heparina', 'ID0002_TEMP');
insert into ponto (nome, xid) values ('[Refrigerador] Divisão', 'ID0001_TEMP');

insert into dado (dado, ts, ponto_id) values (21.22, '2022-01-01 00:02:59', 1);
insert into dado (dado, ts, ponto_id) values (65.97, '2022-01-01 00:00:47', 2);
insert into dado (dado, ts, ponto_id) values (3.93, '2022-01-01 00:03:55', 1);

-- consultas
select p.id, p.nome, d.ts, d.dado
from ponto p 
inner join dado d on d.ponto_id = p.id
where p.id = 1;

select p.id, p.nome, d.ts, d.dado
from ponto p 
inner join dado d on d.ponto_id = p.id
where p.id = 1 and d.ts = '2022-01-01 00:02:59';

SELECT * FROM ponto;
SELECT * FROM dado;


